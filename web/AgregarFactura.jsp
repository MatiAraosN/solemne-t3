<%-- 
    Document   : AgregarFactura
    Created on : 22-sep-2019, 3:04:03
    Author     : MaTii214
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Agregar Factura</title>
    </head>
     <%
        //CONECTAR BDD
        Connection con;
        String url ="jdbc:mysql://localhost:3306/asuorden";
        String Driver="com.mysql.jdbc.Driver";
        String user="root";
        String clave="MaTiiXx214";
        Class.forName(Driver);
        con=DriverManager.getConnection(url,user,clave);
        PreparedStatement ps;
        ResultSet rs;
        ps=con.prepareStatement("select * from cliente");
        rs=ps.executeQuery();
        String folio, fecha, rut, descripcion, neto,iva, total, estado;
        int iva1, total1;
        folio=request.getParameter("txtFolio");
        fecha=request.getParameter("txtFecha");
        rut=request.getParameter("txtRut");
        descripcion=request.getParameter("txtDescripcion");
        neto=request.getParameter("txtNeto");
        estado=request.getParameter("txtEstado");
        if(folio!=null && fecha!=null && rut!=null && descripcion!=null && neto!=null && estado!=null){
            iva1=(int)(Integer.parseInt(neto)*0.19);
            total1=iva1+Integer.parseInt(neto);
            iva = Integer.toString(iva1);
            total = Integer.toString(total1);
            ps=con.prepareStatement("insert into factura(folio, fecha, rut, descripcion, neto, iva, total, estado_pago)values('"+folio+"','"+fecha+"','"+rut+"','"+descripcion+"','"+neto+"','"+iva+"','"+total+"','"+estado+"')");
            ps.executeUpdate();
            response.sendRedirect("CrudFacturas.jsp");
       }
%>
    <body>
        <div class="container">
            <h1>Agregar Registro</h1>
            <hr>
            <form action="" method="post" class="form-control" style="width: 500px; height: 550px">
                Folio:
                <input type="text" name="txtFolio" class="form-control"/>
                Fecha:
                <input type="text" name="txtFecha" class="form-control"/>
                Rut:
                <select name="txtRut" class="form-control">
                    <option value="-1">Seleccionar Rut</option>
                    <% 
                        while(rs.next()){
                            %>
                            <option value="<%=rs.getInt("rut")%>"><%=rs.getInt("rut")%></option>
                        <%}%>
                    %>
                </select>
                Descripcion:
                <input type="text" name="txtDescripcion" class="form-control"/>
                Neto:
                <input type="text" name="txtNeto" class="form-control"/>
                Estado Pago:
                <select name="txtEstado" class="form-control">
                    <option value="-1">Seleccionar Estado</option>
                    <option value="1">Pagado</option>
                    <option value="2">No Pagado</option>
                </select>
                <br>
                <input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>
                <a href="CrudFacturas.jsp">Regresar</a>
            </form>
        </div>
        
        
    </body>
</html>

