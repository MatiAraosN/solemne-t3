<%-- 
    Document   : EditarCliente
    Created on : 22-sep-2019, 1:20:38
    Author     : MaTii214
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Editar Cliente</title>
    </head>
    <body>
        <%
        //CONECTAR BDD
        Connection con;
        String url ="jdbc:mysql://localhost:3306/asuorden";
        String Driver="com.mysql.jdbc.Driver";
        String user="root";
        String clave="MaTiiXx214";
        Class.forName(Driver);
        con=DriverManager.getConnection(url,user,clave);
        //Id seleccionado
        PreparedStatement ps;
        ResultSet rs;
        int rut=Integer.parseInt(request.getParameter("id"));
        ps=con.prepareStatement("select * from cliente where rut="+rut);
        rs=ps.executeQuery();
        while(rs.next()){
           
        %>
        <div class="container">
            <h1>Modificar Registro</h1>
            <hr>
            <form action="" method="post" class="form-control" style="width: 500px; height: 400px">
                Rut:
                <input type="text" readonly="" class="form-control" value="<%= rs.getInt("rut")%>"/>
                Nombre:
                <input type="text" name="txtNombre" class="form-control" value="<%= rs.getString("nombre")%>"/>
                Direccion:
                <input type="text" name="txtDireccion" class="form-control" value="<%= rs.getString("direccion")%>"/>
                Celular:
                <input type="text" name="txtCelular" class="form-control" value="<%= rs.getString("celular")%>"/>
                <br>
                <input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>
                <a href="CrudClientes.jsp">Regresar</a>
            </form>
            <%}%>
        </div>
    </body>
</html>
<% 
        String nom, dir, cel;
        nom=request.getParameter("txtNombre");
        dir=request.getParameter("txtDireccion");
        cel=request.getParameter("txtCelular");
        if(nom!=null && dir!=null && cel!=null){
            ps=con.prepareStatement("update cliente set nombre='"+nom+"',direccion='"+dir+"',celular='"+cel+"'where rut="+rut);
            ps.executeUpdate();
            response.sendRedirect("CrudClientes.jsp");
       }
%>
