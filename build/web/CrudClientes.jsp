<%-- 
    Document   : index
    Created on : 21-sep-2019, 23:51:44
    Author     : MaTii214
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Lista de Clientes</title>
    </head>
    <body>
        <%
        //CONECTAR BDD
        Connection con;
        String url ="jdbc:mysql://localhost:3306/asuorden";
        String Driver="com.mysql.jdbc.Driver";
        String user="root";
        String clave="MaTiiXx214";
        Class.forName(Driver);
        con=DriverManager.getConnection(url,user,clave);
        //Tablas
        PreparedStatement ps;
        ResultSet rs;
        ps=con.prepareStatement("select * from cliente");
        rs=ps.executeQuery();
        %>
        <div class="container">
            <h1>A su Orden! Lista Clientes</h1>
            <hr>
            <a class="btn btn-success btn-lg" href="AgregarCliente.jsp">Nuevo Registro</a>
            <br>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th class="text-center">RUT</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Direccion</th>
                    <th class="text-center">Celular</th>
                    <th class="text-center">Acciones</th>
                </tr>
                <%
                    while(rs.next()){
                %>
                <tr>
                    <td class="text-center"><%= rs.getInt("rut")%></td>
                    <td class="text-center"><%= rs.getString("nombre")%></td>
                    <td class="text-center"><%= rs.getString("direccion")%></td>
                    <td class="text-center"><%= rs.getInt("celular")%></td>
                    <td class="text-center">
                        <a href="EditarCliente.jsp?id=<%= rs.getInt("rut")%>" class="btn btn-warning btn-sm">Editar</a>
                        <a href="EliminarCliente.jsp?id=<%= rs.getInt("rut")%>" class="btn btn-danger btn-sm">Eliminar</a>
                    </td>
                </tr>
                <%}%>
            </table>
        </div>
    </body>
</html>
