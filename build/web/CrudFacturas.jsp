<%-- 
    Document   : CrudFacturas
    Created on : 22-sep-2019, 2:56:18
    Author     : MaTii214
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Lista de Facturas</title>
    </head>
    <body>
        <%
        //CONECTAR BDD
        Connection con;
        String url ="jdbc:mysql://localhost:3306/asuorden";
        String Driver="com.mysql.jdbc.Driver";
        String user="root";
        String clave="MaTiiXx214";
        Class.forName(Driver);
        con=DriverManager.getConnection(url,user,clave);
        //Tablas
        PreparedStatement ps;
        ResultSet rs;
        ps=con.prepareStatement("select * from factura");
        rs=ps.executeQuery();
        %>
        <div class="container">
            <h1>A su Orden! Lista Facturas</h1>
            <hr>
            <a class="btn btn-success btn-lg" href="AgregarFactura.jsp">Nueva Factura</a>
            <br>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th class="text-center">Folio</th>
                    <th class="text-center">Fecha</th>
                    <th class="text-center">Rut</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center">Neto</th>
                    <th class="text-center">IVA</th>
                    <th class="text-center">Total</th>
                    <th class="text-center">Estado Pago</th>
                    <th class="text-center">Acciones</th> 
                </tr>
                <%
                    while(rs.next()){
                %>
                <tr>
                    <td class="text-center"><%= rs.getInt("folio")%></td>
                    <td class="text-center"><%= rs.getInt("fecha")%></td>
                    <td class="text-center"><%= rs.getInt("rut")%></td>
                    <td class="text-center"><%= rs.getString("descripcion")%></td>
                    <td class="text-center"><%= rs.getInt("neto")%></td>
                    <td class="text-center"><%= rs.getInt("iva")%></td>
                    <td class="text-center"><%= rs.getInt("total")%></td>
                    <td class="text-center"><%= rs.getString("estado_pago")%></td>
                    <td class="text-center">
                        <a href="EditarFactura.jsp?id=<%= rs.getInt("folio")%>" class="btn btn-warning btn-sm">Editar</a>
                        <a href="EliminarFactura.jsp?id=<%= rs.getInt("folio")%>" class="btn btn-danger btn-sm">Eliminar</a>
                    </td>
                </tr>
                <%}%>
            </table>
        </div>
    </body>
</html>

